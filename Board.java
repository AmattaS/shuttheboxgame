public class Board {
    //fields
    private Die die1;
    private Die die2;
    private boolean[] closedTiles;
    //Constructor 
    public Board() {
        this.die1 = new Die();
        this.die2 = new Die();
        this.closedTiles = new boolean[12];
    }
    public String toString() {
        String str = "";
        for (int i = 0; i < this.closedTiles.length; i++) {
            if (this.closedTiles[i] != true) {
                System.out.print(i + 1 + ",");

            } else if (this.closedTiles[i] = true) {
                System.out.print("X,");
            }
        }
        return str;
    }
    public boolean playATurn() {
        // Rolling the die
        this.die1.roll();
        this.die2.roll();
        int d1 = this.die1.getPips();
        int d2 = this.die2.getPips();
        System.out.println(d1);
        System.out.println(d2);
        int sum = d1 + d2;
		//setting up booleans 
        boolean res = false;
        boolean arrRes = false;
        // Verifications 
        if (this.closedTiles[sum - 1] == true) {
            System.out.println("This tile is already shut");
            res = true;
        } else if (this.closedTiles[sum - 1] == false) {
            System.out.println("Closing tile: " + (sum));
            arrRes = true;
            res = false;
        }
        // setting array 
        closedTiles[sum - 1] = arrRes;
        return res;
    }
}