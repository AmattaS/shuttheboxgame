import java.util.Random; 
public class Die {
	private int pips;
	private Random dice;
	//Constructors:
	public Die() {
		this.pips = 1;
		this.dice = new Random();
	}
	//get methods:
	public int getPips() {
		return this.pips;
	}
	public Random getDice() {
		return this.dice;
	}
	//instance method:
	public void roll(){
		this.pips = dice.nextInt(6)+1;
	}
	//ToString method
	public String toString(){
		return ""+this.pips;
	}
}