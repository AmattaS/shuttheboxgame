import java.util.Scanner;
public class ShutTheBox
{
	public static void main(String[] args)
	{
		System.out.println("Shut The Box");
		Board game = new Board();
		//gameover boolean 
		boolean gameOver = false;
		
		//gameplay: 
		while(gameOver != true)
		{
			//P1
			System.out.println("Player 1's turn: ");
			System.out.println(game);
			boolean result = game.playATurn();
			//P2 Wins
			if (result)
			{
				System.out.println("Player 2 Wins");
				gameOver = true;
			}
			//No One Wins
			else{
				System.out.println("Player 2's Turn: ");
			}
			System.out.println(game);
			
			boolean altResult = game.playATurn();
			//P1 Wins 
			if (altResult)
			{
				System.out.println("Player 1 Wins");
				gameOver = true;
			}
		}	
	}
}